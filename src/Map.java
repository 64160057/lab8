public class Map {
    private int width;
    private int height;

    public Map(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public Map() {
        this(5, 5);
    }

    public void print() {
        for (int y=1; y <= height; y++) {
            for (int x=1; x<= width; x++) {
                System.out.print("-");
            }
            System.out.println();
        }
    }

    public void setWidth(int width) { //Setter
        this.width = width;
    } 

    public int getWidth() { // Getter
        return width;
    }

    public int getHeight() { // Getter
        return height;
    }
}
