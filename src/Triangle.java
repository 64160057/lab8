public class Triangle {
    // Attributes
    private double a;
    private double b;
    private double c;

    public Triangle(double a, double b, double c) { // Constructor
        this.a = a;
        this.b = b;
        this.c = c;
    }

    // Methodes
    public double printAreaTriangle() {
        double s = (a+b+c)/2;
        double areanosq = s * (s-a) * (s-b) * (s-c);
        double area = Math.sqrt(areanosq);
        System.out.println("Calculate area" + " = " + area);
        return area;
    }

    public double printPerimeterTriangle() {
        double perimeter = a+b+c;
        System.out.println("Calculate perimeter" + " = " + perimeter);
        return perimeter;
    }

    public void seta(double a) { // Setter Methods
        this.a = a;
    }

    public double geta() { //Getter Methods
        return a;
    }

    public double getb() {
        return b;
    }

    public double getc() {
        return c;
    }    
}
